include_directories (doctest)
include_directories (../include)

add_executable (tests main.cpp str_split_tests.cpp)

target_link_libraries(tests PRIVATE clunker)