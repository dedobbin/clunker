#pragma once

#if LOG_LEVEL > 0
    #include <stdio.h> // heh
	#define log_debug(...) printf("[debug] "); printf(__VA_ARGS__); printf("\n")
#else
    #define log_debug(...)
#endif
#if LOG_LEVEL > 1
    #define log_info(...) printf("[info] "); printf(__VA_ARGS__); printf("\n")
#else
    #define log_info(...)
#endif
#if LOG_LEVEL > 2
    #define log_warning(...) printf("[warning] "); printf(__VA_ARGS__); printf("\n")
#else
    #define log_warning(...)
#endif
#if LOG_LEVEL > 3
    #define log_error(...) printf("[error] "); printf(__VA_ARGS__); printf("\n")
#else
    #define log_error(...)
#endif
