#pragma once

#include <vector>
#include <string>

namespace clunker
{
    /**
     * Splits a string.
     *
     * @param input String to be split.
     * @param delim On which to split.
     * @param max_segments The maximum number of entries in result. When negative, goes limitless.
     * @return The segments of the string that was split.
     */
    std::vector<std::string> str_split(const std::string& input, const std::string& delim, int max_segments = -1);
}