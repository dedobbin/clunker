#include "clunker.h"
#include <iostream>
#include <termios.h>
#include <unistd.h>
#include <algorithm>

std::vector<std::string> clunker::str_split(const std::string& input, const std::string& delim, int max_segments)
{
    std::vector<std::string> result;
    if (max_segments == 0) return result;
    
    int start = 0;
    auto end = input.find(delim);
    while (end != std::string::npos){

        if (max_segments == result.size()+1){
            break;
        }

        result.push_back(input.substr(start, end - start));
        start = end + delim.length();
        end = input.find(delim, start);
    }

    result.push_back(input.substr(start));
    return result;
}

char clunker::get_ch() {
    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0)
            perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
            perror("tcsetattr ICANON");
    if (read(0, &buf, 1) < 0)
            perror ("read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
            perror ("tcsetattr ~ICANON");
    return (buf);
}

std::vector<std::pair<int, int>> clunker::get_points_between(int x1, int y1, int x2, int y2)
{
    // thanks brad
    // https://stackoverflow.com/questions/25837544/get-all-points-of-a-straight-line-in-python
    // bresenhamsLineGeneration
    std::vector<std::pair<int, int>> points;
    bool issteep = (abs(y2 - y1) > abs(x2 - x1));
    if (issteep) {
        std::swap(x1, y1);
        std::swap(x2, y2);
    }
    bool rev = false;
    if (x1 > x2) {
        std::swap(x1, x2);
        std::swap(y1, y2);
        rev = true;
    }
    int deltax = x2 - x1;
    int deltay = abs(y2 - y1);
    int error  = int(deltax / 2);
    int y      = y1;
    int ystep;
    if (y1 < y2) {
        ystep = 1;
    } else {
        ystep = -1;
    }

    for (int x = x1; x < x2 + 1; ++x) {
        if (issteep) {
            std::pair<int, int> pt = std::make_pair(y, x);
            points.emplace_back(pt);
        } else {
            std::pair<int, int> pt = std::make_pair(x, y);
            points.emplace_back(pt);
        }

        error -= deltay;
        if (error < 0) {
            y += ystep;
            error += deltax;
        }
    }
    // Reverse the list if the coordinates were reversed
    if (rev) {
        std::reverse(points.begin(), points.end());
    }
    
    return points;
}

int clunker::get_slope(int x1, int y1, int x2, int y2)
{
    if (x1 == x2)
        return 0;

    return (y1 - y2) / (x1 - x2);
}
